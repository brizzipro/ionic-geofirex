import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs";

import * as geofirex from "geofirex";
import * as firebaseApp from "firebase/app";


@Component({
  selector: 'app-cercanos',
  templateUrl: './cercanos.page.html',
  styleUrls: ['./cercanos.page.scss'],
})
export class CercanosPage implements OnInit {

  geo = geofirex.init(firebaseApp);
  points: Observable<any>;
  collection: any;


  constructor() {}

  ngOnInit() {
    this.findData();
  }


	findData() {
    const center = this.geo.point(41.41673625596859, 2.1767253466434884);
    const radius = 15;
    const field = 'position';

    this.points = this.collection = this.geo.collection("positions").within(center, radius, field);
	}
	

  
    findData3() {
	const center = this.geo.point(41.41673625596859, 2.1767253466434884);
    const radius = 15;
    const field = 'position';
	this.points = this.collection = this.geo.collection('positions', ref =>
	ref.where('status', '==', 'online')
	).within(center, radius, field);
	}
	
}
