import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CercanosPage } from './cercanos.page';

describe('CercanosPage', () => {
  let component: CercanosPage;
  let fixture: ComponentFixture<CercanosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CercanosPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CercanosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
