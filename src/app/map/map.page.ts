import { Component, OnInit, ViewChild } from "@angular/core";
import { Observable, BehaviorSubject, pipe, interval } from "rxjs";
import { switchMap, debounceTime, distinctUntilChanged } from "rxjs/operators";

import * as geofirex from "geofirex";
import * as firebaseApp from "firebase/app";

import { AgmMap } from '@agm/core';

@Component({
  selector: "app-map",
  templateUrl: "./map.page.html",
  styleUrls: ["./map.page.scss"]
})
export class MapPage implements OnInit {
	interval;
	docId;
	
  geo = geofirex.init(firebaseApp);
  points: Observable<any>;
  centerPoint = { latitude: 41.41673625596859, longitude: 2.1767253466434884 };
  centerPointSubject = new BehaviorSubject<any>(this.centerPoint);
  radius = 5;
  radiusSubject = new BehaviorSubject<number>(this.radius);
  counter = 0;
  filter = false;
  field = "position";
  zoom = 6;
  collection: any;
  maxRadius = 15;
  maxRequest = 30;

  @ViewChild(AgmMap) agmMap;

  constructor() {}

  ngOnInit() {

    this.collection = this.geo.collection("positions");
    this.findData();
  }

  stopgeo() {
   clearInterval(this.interval);
}
  startgeo() {
  this.interval = setInterval(()=>{
    let lat = 41.3818 + this.rand();
    let lng = 2.1685 + this.rand();
    this.docId = 'fJZZClcjhDV7354pl50MRDfmW4Y2';
    const point = this.geo.point(lat, lng);
    const data = { position: point.data};
    this.collection.setPoint(this.docId, "position", lat, lng);
	console.log(point);
	console.log(data); },5000); 
  }
    rand() {
    const arr = [0.01, -0.01];
    return arr[Math.floor(Math.random() * arr.length)];
  }
  
  findData(tipe?) {
    this.points = this.centerPointSubject.pipe(
      // distinctUntilChanged(),
      switchMap(centerPoint => {
        console.log("filtro por las ubicacion");
        this.counter =  this.counter + 1;
        if (this.counter <= this.maxRequest) {
          return this.geo
            .collection("positions")
            .within(
              this.geo.point(
                this.centerPoint.latitude,
                this.centerPoint.longitude
              ),
              this.radius,
              this.field
            );
        } else {
          if(this.counter > this.maxRequest + 3){
            /**
             * FIXME:lanzar advertencia de exceso de uso
             */
            console.log("te restan 2 oportunidades")
            this.centerPoint = centerPoint;
            return this.geo
              .collection("positions")
              .within(
                this.geo.point(
                  this.centerPoint.latitude,
                  this.centerPoint.longitude
                ),
                this.radius,
                this.field
              );
          }else{
                     /**
             * FIXME: Bloquear
             */
          this.filter = true;
          if(this.counter>this.maxRequest + 5){
            console.log("Te voy a bloquear")
          }
          }
        }
      })
    );
  }
  createPoint(lat, lng) {
    let no = Math.floor(Math.random() * 100) + 101;
    this.collection.setPoint("fJZZClcjhDV7354pl50MRDfmW4Y2", "position", lat, lng);

    // const point = this.geo.point(lat, lng);
    // collection.setDoc("my-place1", { position: point.data });
  }

  show(point) {
    console.log(point);
  }

  updateRadio(r) {
    this.radiusSubject.next(r);
  }

  centerPointDragEnd($event) {
    this.filter = true;
    this.centerPoint = {
      latitude: $event.coords.lat,
      longitude: $event.coords.lng
    };
    this.centerPointSubject.next(this.centerPoint);
  }

  mapClick(evento) {
    console.log(evento);
  }

  clickedCentro(centro) {
    console.log(centro);
  }

  mostrarMarker(m, $event) {
    console.log(m);
  }

  mapClicked($event) {
    this.createPoint($event.coords.lat, $event.coords.lng);
    // console.log($event.coords.lat, $event.coords.lng);
  }

  updateRadius(radius) {
    if (radius <= this.maxRadius) {
      this.radius = radius;
      this.radiusSubject.next(this.radius);
    }
  }
  updateCenter($event) {
    console.log($event);
    this.centerPointSubject.next({
      latitude: $event.coords.lat,
      longitude: $event.coords.lng
    });
  }
}
